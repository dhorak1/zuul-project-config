# project-config

Software Factory configuration for the CentOS Stream project.

Here are configured:

* resources/: Git repositories to be listened by Zuul.
* zuul.d/: Zuul pipelines and base Zuul jobs for CentOS Stream. 
* playbooks/: Ansible playbooks used by Zuul jobs.
